# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .row_factory.RowFactory import FoxtrotRowFactory


class DeltaListBox(Gtk.ListBox, DeltaEntity):

    def _create_widget_func(self, data_item):
        return self._factory.make(self, data_item)

    def _match_func(self, data_item, category):
        return data_item.props.category == category

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListBox.__init__(self, selection_mode=Gtk.SelectionMode.NONE)
        self.add_css_class("boxed-list")
        model = self._enquiry("delta > model")
        category = self._enquiry("delta > category")
        filter_ = Gtk.CustomFilter.new(self._match_func, category)
        category_model = Gtk.FilterListModel.new(model, filter_)
        self._factory = FoxtrotRowFactory()
        self.bind_model(category_model, self._create_widget_func)
        self._raise("delta > add to container", self)
