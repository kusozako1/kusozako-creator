# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys
from .Image import DeltaImage
from .SelectButton import DeltaSelectButton


class DeltaIconRow(DeltaEntity, Gtk.Box):

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)
        return instance

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def construct(self, data_item):
        label = Gtk.Label(xalign=1)
        label.set_text(Keys.TRANSLATABLE_TITLES[data_item.props.key])
        label.set_size_request(160, -1)
        self.append(label)
        self.append(Gtk.Box(hexpand=True))
        DeltaImage(self)
        DeltaSelectButton(self)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=False,
            spacing=16,
            )
        self.set_size_request(-1, 48)
