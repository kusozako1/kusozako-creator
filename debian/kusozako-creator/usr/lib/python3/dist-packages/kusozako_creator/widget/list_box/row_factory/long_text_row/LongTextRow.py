# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ListBoxRow import AlfaListBoxRow
from .Text import DeltaText


class DeltaLongTextRow(AlfaListBoxRow):

    def _on_construct(self, data_item):
        DeltaText.new(self, data_item)
