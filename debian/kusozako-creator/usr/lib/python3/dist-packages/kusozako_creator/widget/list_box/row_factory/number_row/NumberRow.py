# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ListBoxRow import AlfaListBoxRow
from .SpinButton import DeltaSpinButton


class DeltaNumberRow(AlfaListBoxRow):

    def _on_construct(self, data_item):
        DeltaSpinButton.new(self, data_item)
