# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals


class DeltaSpinButton(Gtk.SpinButton, DeltaEntity):

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)
        return instance

    def _on_changed(self, spin_button, key=None):
        param = key, str(spin_button.get_value_as_int())
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def construct(self, data_item):
        self.connect("changed", self._on_changed, data_item.props.key)
        self.set_text(data_item.props.value)
        param = data_item.props.key, "0"
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SpinButton.__init__(
            self,
            digits=0,
            climb_rate=1,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            hexpand=True,
            )
        adjustment = self.get_adjustment()
        adjustment.set_step_increment(1)
        adjustment.set_lower(0)
        adjustment.set_upper(10)
        self._raise("delta > add to container", self)
