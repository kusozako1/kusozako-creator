# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_creator.const import Keys
from kusozako_creator.const import BufferSignals


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _change_value(self, buffer_):
        for key, value in buffer_.items():
            if key == Keys.PROJECT_DIRECTORY:
                shorten_path = HomeDirectory.shorten(value)
                self.set_text(shorten_path)

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.BUFFER_CHANGED:
            return
        self._change_value(buffer_)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, hexpand=True, xalign=1)
        self._raise("delta > add to container", self)
        self._raise("delta > register buffer object", self)
