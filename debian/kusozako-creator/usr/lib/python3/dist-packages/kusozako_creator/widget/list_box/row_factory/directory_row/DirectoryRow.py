# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ListBoxRow import AlfaListBoxRow
from kusozako_creator.const import BufferSignals
from .Label import DeltaLabel
from .SelectButton import DeltaSelectButton


class DeltaDirectoryRow(AlfaListBoxRow):

    def _on_construct(self, data_item):
        param = data_item.props.key, ""
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)
        DeltaLabel(self)
        DeltaSelectButton(self)
