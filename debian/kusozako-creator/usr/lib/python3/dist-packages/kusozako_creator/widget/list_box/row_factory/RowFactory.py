# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import DataTypes
from .icon_row.IconRow import DeltaIconRow
from .directory_row.DirectoryRow import DeltaDirectoryRow
from .short_text_row.ShortTextRow import DeltaShortTextRow
from .long_text_row.LongTextRow import DeltaLongTextRow
from .number_row.NumberRow import DeltaNumberRow
from .user_directories_row.UserDirectoriesRow import DeltaUserDirectoriesRow

METHOD_NAMES = {
    DataTypes.ICON_PATH: "_get_icon_row",
    DataTypes.DIRECTORY_PATH: "_get_directory_row",
    DataTypes.LONG_TEXT: "_get_long_text_row",
    DataTypes.SHORT_TEXT: "_get_short_text_row",
    DataTypes.NUMBER: "_get_number_row",
    DataTypes.USER_DIRECTORIES: "_get_user_directories_row",
}


class FoxtrotRowFactory:

    def _get_icon_row(self, parent, data_item):
        return DeltaIconRow.new(parent, data_item)

    def _get_directory_row(self, parent, data_item):
        return DeltaDirectoryRow.new(parent, data_item)

    def _get_long_text_row(self, parent, data_item):
        return DeltaLongTextRow.new(parent, data_item)

    def _get_short_text_row(self, parent, data_item):
        return DeltaShortTextRow.new(parent, data_item)

    def _get_number_row(self, parent, data_item):
        return DeltaNumberRow.new(parent, data_item)

    def _get_user_directories_row(self, parent, data_item):
        return DeltaUserDirectoriesRow.new(parent, data_item)

    def make(self, parent, data_item):
        type_ = data_item.props.type
        method_name = METHOD_NAMES[type_]
        method = getattr(self, method_name)
        return method(parent, data_item)
