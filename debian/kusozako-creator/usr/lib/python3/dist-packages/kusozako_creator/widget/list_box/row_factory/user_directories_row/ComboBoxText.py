# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_creator.const import BufferSignals

NAMES = {
    0: _("Desktop : "),
    1: _("Documents : "),
    2: _("Download : "),
    3: _("Music : "),
    4: _("Pictures : "),
    5: _("Public Share : "),
    6: _("Templates : "),
    7: _("Videos : "),
}


class DeltaComboBoxText(Gtk.ComboBoxText, DeltaEntity):

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)
        return instance

    def _on_changed(self, combo_box_text, key):
        param = key, combo_box_text.get_active_id()
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def _set_user_special_dirs(self):
        for index in range(0, GLib.UserDirectory.N_DIRECTORIES):
            id_ = str(index)
            path = GLib.get_user_special_dir(index)
            value = NAMES[index]+HomeDirectory.shorten(path)
            self.append(id_, value)

    def construct(self, data_item):
        self.connect("changed", self._on_changed, data_item.props.key)
        self.append("-1", "Home: ~/")
        self._set_user_special_dirs()
        self.set_active_id("-1")
        param = data_item.props.key, "-1"
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ComboBoxText.__init__(
            self,
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            hexpand=True,
            )
        self._raise("delta > add to container", self)
