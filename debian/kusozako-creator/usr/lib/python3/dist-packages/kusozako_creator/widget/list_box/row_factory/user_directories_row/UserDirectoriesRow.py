# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ListBoxRow import AlfaListBoxRow
from .ComboBoxText import DeltaComboBoxText


class DeltaUserDirectoriesRow(AlfaListBoxRow):

    def _on_construct(self, data_item):
        DeltaComboBoxText.new(self, data_item)
