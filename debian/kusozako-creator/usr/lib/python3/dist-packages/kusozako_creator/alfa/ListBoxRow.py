# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class AlfaListBoxRow(DeltaEntity, Gtk.Box):

    __label_width__ = 160

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)
        return instance

    def _delta_call_add_to_container(self, widget):
        self.append(widget)

    def _on_construct(self, data_item):
        raise NotImplementedError

    def construct(self, data_item):
        label = Gtk.Label(xalign=1)
        label.set_text(Keys.TRANSLATABLE_TITLES[data_item.props.key])
        label.set_size_request(self.__label_width__, -1)
        self.append(label)
        self._on_construct(data_item)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.HORIZONTAL,
            homogeneous=False,
            spacing=16,
            )
        self.set_size_request(-1, 48)
