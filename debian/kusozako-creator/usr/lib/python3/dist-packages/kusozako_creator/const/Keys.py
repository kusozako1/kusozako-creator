# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

DEVELOPPER_NAME = "developper-name"
DEVELOPPER_EMAIL = "developper-email"
PROJECT_NAME = "project-name"
PROJECT_ID = "project-id"
PROJECT_ICON = "project-icon"
PROJECT_WEB_PAGE = "project-web-page"
PROJECT_DIRECTORY = "project-directory"
FILES_MIN = "files-min"
FILES_MAX = "files-max"
FILES_DEFAULT_DIRECTORY = "files-default-directory"
FILES_MIME = "files-mime"
DESCRIPTION_SHORT = "description-short"
DESCRIPTION_LONG = "description-long"


TRANSLATABLE_TITLES = {
    DEVELOPPER_NAME: _("Developper Name"),
    DEVELOPPER_EMAIL: _("EMail"),
    PROJECT_NAME: _("Name"),
    PROJECT_ID: _("ID"),
    PROJECT_ICON: _("Icon"),
    PROJECT_WEB_PAGE: _("Web Page URL"),
    PROJECT_DIRECTORY: _("Directory"),
    FILES_MIN: _("Minimum Files"),
    FILES_MAX: _("Maxium Files"),
    FILES_DEFAULT_DIRECTORY: _("Default Directory"),
    FILES_MIME: _("MIME Type"),
    DESCRIPTION_SHORT: _("Short Description"),
    DESCRIPTION_LONG: _("Long Description"),
}
