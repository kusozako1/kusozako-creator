# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

CHANGE_BUFFER = "change-buffer"             # key, value
BUFFER_CHANGED = "buffer-changed"           # whole buffer
QUEUE_BUFFER = "queue-buffer"               # None
BUFFER_APPENDED = "buffer-appended"         # message as str
CREATION_FINISHED = "creation-finished"     # None
