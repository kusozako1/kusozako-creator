# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import TemplateFiles
from kusozako_creator.const import BufferSignals
from .SourcePaths import DeltaSourcePaths
from .TargetPaths import DeltaTargetPaths

MESSAGE = _("directory created to {}")


class DeltaTemplate(DeltaEntity):

    def _make_directory(self, path):
        gfile = Gio.File.new_for_path(path)
        gfile.make_directory(None)
        message = MESSAGE.format(path)
        user_data = BufferSignals.BUFFER_APPENDED, message
        self._raise("delta > buffer signal", user_data)

    def enumerate_paths(self):
        source_paths = DeltaSourcePaths(self)
        target_paths = DeltaTargetPaths(self)
        for name in TemplateFiles.NAMES:
            is_directory, source_path = source_paths.path_for_name(name)
            target_path = target_paths.path_for_name(name)
            if is_directory:
                self._make_directory(target_path)
            else:
                yield source_path, target_path

    def __init__(self, parent):
        self._parent = parent
