# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SpdxLicense import DeltaSpdxLicense
from .LibraryName import DeltaLibraryName
from .BinName import DeltaBinName
from .Exec import DeltaExec
from .EntryPointName import DeltaEntryPointName
from .YYYY import DeltaYYYY
from .YYYYMMDD import DeltaYYYYMMDD
from .DateFull import DeltaDateFull
from .ApplicationId import DeltaApplicationId
from .ShortDescription import DeltaShortDescription
from .LongDescription import DeltaLongDescription
from .ControlDescription import DeltaControlDescription
from .DevelopperName import DeltaDevelopperName
from .DevelopperEMail import DeltaDevelopperEMail
from .MinFiles import DeltaMinFiles
from .MaxFiles import DeltaMaxFiles
from .MimeList import DeltaMimeList
from .MimeType import DeltaMimeType
from .DefaultDirectory import DeltaDefaultDirectory


class EchoUnits:

    def __init__(self, parent):
        DeltaSpdxLicense(parent)
        DeltaLibraryName(parent)
        DeltaBinName(parent)
        DeltaExec(parent)
        DeltaEntryPointName(parent)
        DeltaYYYY(parent)
        DeltaYYYYMMDD(parent)
        DeltaDateFull(parent)
        DeltaApplicationId(parent)
        DeltaShortDescription(parent)
        DeltaLongDescription(parent)
        DeltaControlDescription(parent)
        DeltaDevelopperName(parent)
        DeltaDevelopperEMail(parent)
        DeltaMinFiles(parent)
        DeltaMaxFiles(parent)
        DeltaMimeList(parent)
        DeltaMimeType(parent)
        DeltaDefaultDirectory(parent)
