# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.Transmitter import FoxtrotTransmitter
from .buffer.Buffer import DeltaBuffer


class DeltaBufferLayer(DeltaEntity):

    def _delta_call_buffer_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_buffer_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        DeltaBuffer(self)
