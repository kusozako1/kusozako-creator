# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaYYYYMMDD(DeltaEntity):

    __key__ = "@KUSOZAKO_YYYY_MM_DD@"

    def __init__(self, parent):
        self._parent = parent
        year, month, day = GLib.DateTime.new_now_local().get_ymd()
        value = "{}.{:01}.{:01}".format(year, month, day)
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
