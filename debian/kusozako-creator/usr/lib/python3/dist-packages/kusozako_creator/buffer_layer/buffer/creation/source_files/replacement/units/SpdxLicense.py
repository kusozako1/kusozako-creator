# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys

FORMAT = """# (c) copyright {}, {} <{}>
# SPDX-License-Identifier: GPL-3.0-or-later"""


class DeltaSpdxLicense(DeltaEntity):

    __key__ = "@KUSOZAKO_SPDX_LICENSE@"

    def __init__(self, parent):
        self._parent = parent
        year = GLib.DateTime.new_now_local().get_year()
        name = self._enquiry("delta > buffer data", Keys.DEVELOPPER_NAME)
        email = self._enquiry("delta > buffer data", Keys.DEVELOPPER_EMAIL)
        value = FORMAT.format(year, name, email)
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
