# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaSourcePaths(DeltaEntity):

    def path_for_name(self, name):
        path = GLib.build_filenamev([self._directory, name])
        is_directory = GLib.file_test(path, GLib.FileTest.IS_DIR)
        return is_directory, path

    def __init__(self, parent):
        self._parent = parent
        names = [GLib.path_get_dirname(__file__), "data"]
        self._directory = GLib.build_filenamev(names)
