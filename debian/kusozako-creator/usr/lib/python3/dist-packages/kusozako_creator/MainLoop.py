# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.main_loop.MainLoop import AlfaMainLoop
from . import APPLICATION_DATA
from .buffer_layer.BufferLayer import DeltaBufferLayer
from .user_interface.UserInterface import DeltaUserInterface


class DeltaMainLoop(AlfaMainLoop):

    def _delta_call_loopback_buffer_layer_ready(self, parent):
        DeltaUserInterface(parent)

    def _delta_call_loopback_application_window_ready(self, parent):
        DeltaBufferLayer(parent)

    def _delta_info_data(self, key):
        return APPLICATION_DATA.get(key, None)
