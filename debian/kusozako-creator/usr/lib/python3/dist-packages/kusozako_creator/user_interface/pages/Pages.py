# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .developper.Developper import DeltaDevelopper
from .project.Project import DeltaProject
from .files.Files import DeltaFiles
from .description.Description import DeltaDescription
from .create.Create import DeltaCreate


class EchoPages:

    def __init__(self, parent):
        DeltaDevelopper(parent)
        DeltaProject(parent)
        DeltaFiles(parent)
        DeltaDescription(parent)
        DeltaCreate(parent)
