# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject


class FoxtrotItem(GObject.Object):

    @classmethod
    def new(cls, category, key, value, type_):
        instance = cls()
        instance.construct(category, key, value, type_)
        return instance

    def construct(self, category, key, value, type_):
        self._category = category
        self._key = key
        self._value = value
        self._type = type_

    def __init__(self):
        GObject.Object.__init__(self)

    @GObject.Property
    def category(self):
        return self._category

    @GObject.Property
    def key(self):
        return self._key

    @GObject.Property
    def value(self):
        return self._value

    @GObject.Property
    def type(self):
        return self._type
