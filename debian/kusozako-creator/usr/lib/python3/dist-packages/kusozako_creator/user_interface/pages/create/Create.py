# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import Categories
from kusozako_creator.alfa.StackPage import AlfaStackPage
from .action_bar.ActionBar import DeltaActionBar
from .MessageArea import DeltaMessageArea
from .CreationQueue import DeltaCreationQueue


class DeltaCreate(AlfaStackPage):

    __page_name__ = Categories.CREATE
    __title__ = _("Create Project")

    def _set_contents(self):
        DeltaActionBar(self)
        DeltaMessageArea(self)
        DeltaCreationQueue(self)
