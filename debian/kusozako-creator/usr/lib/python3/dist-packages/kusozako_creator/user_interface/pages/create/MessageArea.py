# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals


class DeltaMessageArea(Gtk.Label, DeltaEntity):

    def receive_transmission(self, user_data):
        signal, message = user_data
        if signal != BufferSignals.BUFFER_APPENDED:
            return
        text = self.get_text()+"\n"+message
        self.set_text(text)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            hexpand=True,
            vexpand=True,
            margin_bottom=16,
            )
        Gtk.Label.__init__(
            self,
            label="please wait...",
            hexpand=True,
            vexpand=True,
            margin_start=16,
            )
        scrolled_window.add_css_class("card")
        self.add_css_class("title-4")
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
        self._raise("delta > register buffer object", self)
