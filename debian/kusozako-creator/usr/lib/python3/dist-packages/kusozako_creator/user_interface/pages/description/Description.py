# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import Categories
from kusozako_creator.alfa.StackPage import AlfaStackPage
from kusozako_creator.widget.VerticalSpacer import DeltaVerticalSpacer
from kusozako_creator.widget.list_box.ListBox import DeltaListBox
from .action_bar.ActionBar import DeltaActionBar


class DeltaDescription(AlfaStackPage):

    __page_name__ = Categories.DESCRIPTION
    __title__ = _("Project Description")

    def _set_contents(self):
        DeltaActionBar(self)
        DeltaVerticalSpacer(self)
        DeltaListBox(self)
