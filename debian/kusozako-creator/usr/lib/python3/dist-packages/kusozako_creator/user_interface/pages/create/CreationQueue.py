# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Categories
from kusozako_creator.const import BufferSignals


class DeltaCreationQueue(DeltaEntity):

    def _timeout(self):
        user_data = BufferSignals.QUEUE_BUFFER, None
        self._raise("delta > buffer signal", user_data)
        return GLib.SOURCE_REMOVE

    def _on_notify(self, stack, param_spec):
        if stack.get_visible_child_name() != Categories.CREATE:
            return
        GLib.timeout_add_seconds(1, self._timeout)

    def __init__(self, parent):
        self._parent = parent
        stack = self._enquiry("delta > stack")
        stack.connect("notify::visible-child-name", self._on_notify)
