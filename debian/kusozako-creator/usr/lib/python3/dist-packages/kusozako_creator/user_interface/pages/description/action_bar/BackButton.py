# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import Categories
from kusozako_creator.alfa.BackButton import AlfaBackButton


class DeltaBackButton(AlfaBackButton):

    __previous_category__ = Categories.PROJECT
