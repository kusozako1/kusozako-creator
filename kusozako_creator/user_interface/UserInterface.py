# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from .model.Model import DeltaModel
from .pages.Pages import EchoPages


class DeltaUserInterface(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _delta_info_stack(self):
        return self

    def _delta_info_model(self):
        return self._model

    def __init__(self, parent):
        self._parent = parent
        self._model = DeltaModel(parent)
        Gtk.Stack.__init__(
            self,
            hexpand=True,
            vexpand=True,
            transition_type=Gtk.StackTransitionType.SLIDE_LEFT_RIGHT,
            transition_duration=250,
            )
        self.add_css_class("kusozako-content-area")
        EchoPages(self)
        self._raise("delta > add to container", self)
