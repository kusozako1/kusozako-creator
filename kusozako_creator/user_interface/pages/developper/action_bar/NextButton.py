# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import Categories
from kusozako_creator.alfa.NextButton import AlfaNextButton


class DeltaNextButton(AlfaNextButton):

    __next_category__ = Categories.PROJECT
    __own_category__ = Categories.DEVELOPPER
