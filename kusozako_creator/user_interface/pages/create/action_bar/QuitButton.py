# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako1.const import MainWindowSignals
from kusozako_creator.const import BufferSignals


class DeltaQuitButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        user_data = MainWindowSignals.FORCE_CLOSE, None
        self._raise("delta > main window signal", user_data)

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.CREATION_FINISHED:
            return
        self.set_sensitive(True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Quit"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            sensitive=False,
            )
        self.connect("clicked", self._on_clicked)
        self.add_css_class("destructive-action")
        self._raise("delta > add to container end", self)
        self._raise("delta > register buffer object", self)
