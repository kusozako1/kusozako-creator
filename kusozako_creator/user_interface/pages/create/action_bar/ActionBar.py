# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ActionBar import AlfaActionBar
from .OpenDirectoryButton import DeltaOpenDirectoryButton
from .QuitButton import DeltaQuitButton


class DeltaActionBar(AlfaActionBar):

    __label__ = "5 / 5"

    def _set_contents(self):
        DeltaOpenDirectoryButton(self)
        DeltaQuitButton(self)
