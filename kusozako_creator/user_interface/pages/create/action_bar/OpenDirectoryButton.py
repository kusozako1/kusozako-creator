# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys
from kusozako_creator.const import BufferSignals

SUBPROCESS_FLAG = Gio.SubprocessFlags.NONE


class DeltaOpenDirectoryButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        path = self._enquiry("delta > buffer data", Keys.PROJECT_DIRECTORY)
        subprocess = Gio.Subprocess.new(["xdg-open", path], SUBPROCESS_FLAG)
        subprocess.wait_async(None, None, None)

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.CREATION_FINISHED:
            return
        self.set_sensitive(True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Open Project Directory"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            sensitive=False,
            )
        self.connect("clicked", self._on_clicked)
        self.add_css_class("suggested-action")
        self._raise("delta > add to container start", self)
        self._raise("delta > register buffer object", self)
