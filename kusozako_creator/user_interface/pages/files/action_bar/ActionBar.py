# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ActionBar import AlfaActionBar
from .NextButton import DeltaNextButton
from .BackButton import DeltaBackButton


class DeltaActionBar(AlfaActionBar):

    __label__ = "3 / 5"

    def _set_contents(self):
        DeltaBackButton(self)
        DeltaNextButton(self)
