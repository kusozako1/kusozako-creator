# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Categories
from kusozako_creator.const import BufferSignals


class DeltaCreateButton(Gtk.Button, DeltaEntity):

    __next_category__ = Categories.CREATE
    __own_category__ = Categories.DESCRIPTION

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", self.__next_category__)

    def _get_sensitive(self, buffer_):
        for key, value in buffer_.items():
            if key.startswith(self.__own_category__) and not value:
                return False
        return True

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.BUFFER_CHANGED:
            return
        sensitive = self._get_sensitive(buffer_)
        self.set_sensitive(sensitive)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Create"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            sensitive=False,
            )
        self.connect("clicked", self._on_clicked)
        self.add_css_class("destructive-action")
        self._raise("delta > add to container end", self)
        self._raise("delta > register buffer object", self)
