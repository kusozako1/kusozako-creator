# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ActionBar import AlfaActionBar
from .BackButton import DeltaBackButton
from .CreateButton import DeltaCreateButton


class DeltaActionBar(AlfaActionBar):

    __label__ = "4 / 5"

    def _set_contents(self):
        DeltaBackButton(self)
        DeltaCreateButton(self)
