# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.const import Categories
from kusozako_creator.const import Keys
from kusozako_creator.const import DataTypes


MODEL_DATA = [
    {
        "category": Categories.DEVELOPPER,
        "key": Keys.DEVELOPPER_NAME,
        "load-from-settings": True,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.DEVELOPPER,
        "key": Keys.DEVELOPPER_EMAIL,
        "load-from-settings": True,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.PROJECT,
        "key": Keys.PROJECT_NAME,
        "load-from-settings": False,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.PROJECT,
        "key": Keys.PROJECT_ID,
        "load-from-settings": False,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.PROJECT,
        "key": Keys.PROJECT_ICON,
        "load-from-settings": False,
        "type": DataTypes.ICON_PATH
    },
    {
        "category": Categories.PROJECT,
        "key": Keys.PROJECT_WEB_PAGE,
        "load-from-settings": False,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.PROJECT,
        "key": Keys.PROJECT_DIRECTORY,
        "load-from-settings": False,
        "type": DataTypes.DIRECTORY_PATH
    },
    {
        "category": Categories.FILES,
        "key": Keys.FILES_MIN,
        "load-from-settings": False,
        "type": DataTypes.NUMBER
    },
    {
        "category": Categories.FILES,
        "key": Keys.FILES_MAX,
        "load-from-settings": False,
        "type": DataTypes.NUMBER
    },
    {
        "category": Categories.FILES,
        "key": Keys.FILES_DEFAULT_DIRECTORY,
        "load-from-settings": False,
        "type": DataTypes.USER_DIRECTORIES
    },
    {
        "category": Categories.FILES,
        "key": Keys.FILES_MIME,
        "load-from-settings": False,
        "type": DataTypes.LONG_TEXT
    },
    {
        "category": Categories.DESCRIPTION,
        "key": Keys.DESCRIPTION_SHORT,
        "load-from-settings": False,
        "type": DataTypes.SHORT_TEXT
    },
    {
        "category": Categories.DESCRIPTION,
        "key": Keys.DESCRIPTION_LONG,
        "load-from-settings": False,
        "type": DataTypes.LONG_TEXT
    },
]
