# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako1.Entity import DeltaEntity
from .Item import FoxtrotItem
from .Data import MODEL_DATA


class DeltaModel(Gio.ListStore, DeltaEntity):

    def _get_value(self, item_data):
        if not item_data["load-from-settings"]:
            return ""
        query = item_data["category"], item_data["key"], ""
        value = self._enquiry("delta > settings", query)
        return value

    def _construct(self):
        for item_data in MODEL_DATA:
            data_item = FoxtrotItem.new(
                item_data["category"],
                item_data["key"],
                self._get_value(item_data),
                item_data["type"]
                )
            self.append(data_item)

    def __init__(self, parent):
        self._parent = parent
        Gio.ListStore.__init__(self, item_type=FoxtrotItem)
        self._construct()
