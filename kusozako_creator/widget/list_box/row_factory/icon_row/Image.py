# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys
from kusozako_creator.const import BufferSignals


class DeltaImage(Gtk.Image, DeltaEntity):

    def _change_icon(self, buffer_):
        for key, value in buffer_.items():
            if key == Keys.PROJECT_ICON:
                self.set_from_file(value)

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.BUFFER_CHANGED:
            return
        self._change_icon(buffer_)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Image.__init__(self, margin_top=4, margin_bottom=4, margin_end=4)
        self._raise("delta > add to container", self)
        self._raise("delta > register buffer object", self)
