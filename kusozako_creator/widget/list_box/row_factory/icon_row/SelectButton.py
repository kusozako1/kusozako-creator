# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako1.file_dialog_2.FileDialog import DeltaFileDialog
from kusozako_creator.const import Keys
from kusozako_creator.const import BufferSignals


class DeltaSelectButton(Gtk.Button, DeltaEntity):

    def _delta_call_dialog_response(self, gfile):
        directory = gfile.get_parent().get_path()
        settings_data = "dialog", "icon-last-selected-directory", directory
        self._raise("delta > settings", settings_data)
        param = Keys.PROJECT_ICON, gfile.get_path()
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def _on_clicked(self, button):
        query = "dialog", "icon-last-selected-directory", GLib.get_home_dir()
        directory = self._enquiry("delta > settings", query)
        if not GLib.file_test(directory, GLib.FileTest.EXISTS):
            directory = GLib.get_home_dir()
        DeltaFileDialog.select_file(
            self,
            default_directory=Gio.File.new_for_path(directory),
            mime_types=["image/svg"],
            title=_("Select SVG Icon"),
            )

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Select"),
            margin_top=4,
            margin_bottom=4,
            margin_end=4,
            )
        self.add_css_class("suggested-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
