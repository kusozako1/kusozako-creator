# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals


class DeltaText(Gtk.TextView, DeltaEntity):

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)

    def _on_changed(self, buffer_, key):
        param = key, buffer_.props.text
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def construct(self, data_item):
        buffer_ = self.get_buffer()
        buffer_.connect("changed", self._on_changed, data_item.props.key)
        param = data_item.props.key, ""
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(
            hexpand=True,
            vexpand=True,
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            )
        scrolled_window.set_size_request(-1, 320)
        scrolled_window.add_css_class("kusozako-text-view-container")
        Gtk.TextView.__init__(self)
        self.add_css_class("kusozako-text-view")
        scrolled_window.set_child(self)
        self._raise("delta > add to container", scrolled_window)
