# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_creator.alfa.ListBoxRow import AlfaListBoxRow
from .Entry import DeltaEntry


class DeltaShortTextRow(AlfaListBoxRow):

    def _on_construct(self, data_item):
        DeltaEntry.new(self, data_item)
