# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals


class DeltaEntry(Gtk.Entry, DeltaEntity):

    @classmethod
    def new(cls, parent, data_item):
        instance = cls(parent)
        instance.construct(data_item)
        return instance

    def _on_changed(self, entry, key):
        param = key, entry.get_text()
        user_data = BufferSignals.CHANGE_BUFFER, param
        self._raise("delta > buffer signal", user_data)

    def construct(self, data_item):
        self.connect("changed", self._on_changed, data_item.props.key)
        self.set_text(data_item.props.value)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Entry.__init__(
            self,
            text=" ",
            margin_top=4,
            margin_bottom=4,
            margin_start=4,
            margin_end=4,
            hexpand=True
            )
        self._raise("delta > add to container", self)
