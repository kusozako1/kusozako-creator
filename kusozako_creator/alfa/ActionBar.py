# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaActionBar(Gtk.CenterBox, DeltaEntity):

    __label__ = "define label here."

    def _delta_call_add_to_container_start(self, widget):
        self.set_start_widget(widget)

    def _delta_call_add_to_container_end(self, widget):
        self.set_end_widget(widget)

    def _set_contents(self):
        raise NotImplementedError

    def __init__(self, parent):
        self._parent = parent
        Gtk.CenterBox.__init__(self, margin_bottom=16)
        self.set_center_widget(Gtk.Label(label=self.__label__, hexpand=True))
        self.set_size_request(-1, 48)
        self.add_css_class("card")
        self._set_contents()
        self._raise("delta > add to container", self)
