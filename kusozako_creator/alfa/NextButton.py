# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals


class AlfaNextButton(Gtk.Button, DeltaEntity):

    __next_category__ = "define next category here."
    __own_category__ = "define own category here."

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", self.__next_category__)

    def _get_sensitive(self, buffer_):
        for key, value in buffer_.items():
            if key.startswith(self.__own_category__) and not value:
                return False
        return True

    def receive_transmission(self, user_data):
        signal, buffer_ = user_data
        if signal != BufferSignals.BUFFER_CHANGED:
            return
        sensitive = self._get_sensitive(buffer_)
        self.set_sensitive(sensitive)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Next"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            sensitive=False,
            )
        self.add_css_class("suggested-action")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container end", self)
        self._raise("delta > register buffer object", self)
