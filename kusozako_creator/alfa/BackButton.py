# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaBackButton(Gtk.Button, DeltaEntity):

    __previous_category__ = "define previous category here."

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", self.__previous_category__)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Button.__init__(
            self,
            label=_("Back"),
            margin_start=4,
            margin_end=4,
            margin_top=4,
            margin_bottom=4,
            )
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container start", self)
