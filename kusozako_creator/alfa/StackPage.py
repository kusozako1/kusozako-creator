# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Adw
from gi.repository import Gtk
from kusozako1.Entity import DeltaEntity


class AlfaStackPage(Gtk.ScrolledWindow, DeltaEntity):

    __page_name__ = "define stack page name here."
    __title__ = "define visible title here."

    def _set_header(self):
        label = Gtk.Label(
            label=self.__title__,
            margin_top=16,
            margin_bottom=16
            )
        label.set_size_request(-1, 64)
        label.add_css_class("kusozako-large-title")
        label.add_css_class("card")
        self._box.prepend(label)

    def _set_contents(self):
        raise NotImplementedError

    def _delta_info_category(self):
        return self.__page_name__

    def _delta_call_add_to_container(self, widget):
        self._box.prepend(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ScrolledWindow.__init__(self)
        clamp = Adw.Clamp()
        self.set_child(clamp)
        self._box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        clamp.set_child(self._box)
        self._set_contents()
        self._set_header()
        user_data = self, self.__page_name__
        self._raise("delta > add to stack", user_data)
