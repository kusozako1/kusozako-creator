# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import gettext
import locale

VERSION = "2024.11.22"
APPLICATION_NAME = "kusozako-creator"
APPLICATION_ID = "com.gitlab.kusozako1.Creator"

locale.setlocale(locale.LC_ALL, None)
gettext.install(
    APPLICATION_NAME,
    "/usr/share/locale",
    names=('gettext', 'ngettext')
)

APPLICATION_DATA = {
    "version": VERSION,
    "min-files": 0,
    "max-files": 0,
    "application-name": APPLICATION_NAME,
    "rdnn-name": APPLICATION_ID,
    "application-id": APPLICATION_ID,
    "long-description": "Application creator for kusozako project"
}
