# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

NAMES = (
    ".gitignore",
    "BIN_NAME.PYTHON",
    "meson.build",
    # "README.md",
    "LICENSE",
    "LIBRARY_NAME",
    "LIBRARY_NAME/__init__.PYTHON",
    "LIBRARY_NAME/MainLoop.PYTHON",
    "LIBRARY_NAME/Hello.PYTHON",
    "debian",
    "debian/changelog",
    "debian/copyright",
    "debian/control",
    "debian/rules",
    "debian/source",
    "debian/source/format",
    "data",
    "data/meson.build",
    "data/APPLICATION_ID.desktop",
    # "data/APPLICATION_ID.svg"             load from user defined path
)

TARGETS = (
    "@KUSOZAKO_SPDX_LICENSE@",          # license header file
    "@KUSOZAKO_LIBRARY_NAME@",          # kusozako_alfa
    "@KUSOZAKO_BIN_NAME@",              # kusozako-alfa
    "@KUSOZAKO_ENTRY_POINT_NAME@",      # kusozako-alfa or kusozako
    "@KUSOZAKO_YYYY@",                  # 2023
    "@KUSOZAKO_YYYY_MM_DD@",            # 2023.12.03
    "@KUSOZAKO_DATE_FULL@",             # Sun, 03 Nov 2023
    "@KUSOZAKO_APPLICATION_ID@",        # com.gitlab.kusozako1.Alfa
    "@KUSOZAKO_SHORT_DESCRIPTION@"      # single-line short description
    "@KUSOZAKO_LONG_DESCRIPTION@",      # multi-line long description
    "@KUSOZAKO_CONTROL_DESCRIPTION@",   # control file safe long description
    "@KUSOZAKO_DEVELOPPER_NAME@",       # dev name
    "@KUSOZAKO_DEVELOPPER_EMAIL@",      # dev email address
)
