# (c) copyright 2023-2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

SHORT_TEXT = "short-text"
LONG_TEXT = "long-text"
ICON_PATH = "icon-path"
DIRECTORY_PATH = "directory-path"

# testing

NUMBER = "number"
USER_DIRECTORIES = "user-directories"
STRING_ARRAY = "string-array"
