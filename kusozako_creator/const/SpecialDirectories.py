# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

HOME = "home"

TRANSLATABLE_NAMES = {
    HOME: _("Home"),
}
