# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals
from .creation.Creation import DeltaCreation


class DeltaBuffer(DeltaEntity):

    def _set_buffer(self, key, value):
        self._buffer[key] = value
        user_data = BufferSignals.BUFFER_CHANGED, self._buffer.copy()
        self._raise("delta > buffer signal", user_data)

    def _delta_info_buffer(self):
        return self._buffer.copy()

    def _delta_info_buffer_data(self, key):
        return self._buffer[key]

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal != BufferSignals.CHANGE_BUFFER:
            return
        self._set_buffer(*param)

    def __init__(self, parent):
        self._parent = parent
        self._buffer = {}
        DeltaCreation(self)
        self._raise("delta > register buffer object", self)
        self._raise("delta > loopback buffer layer ready", self)
