# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import BufferSignals
from .source_files.SourceFiles import DeltaSourceFiles
from .Icon import DeltaIcon


class DeltaCreation(DeltaEntity):

    def _create(self):
        DeltaSourceFiles(self)
        DeltaIcon(self)
        user_data = BufferSignals.CREATION_FINISHED, None
        self._raise("delta > buffer signal", user_data)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != BufferSignals.QUEUE_BUFFER:
            return
        self._create()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register buffer object", self)
