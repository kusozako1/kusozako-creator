# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys
from kusozako_creator.const import BufferSignals

MESSAGE = _("Icon file copied to {}")


class DeltaIcon(DeltaEntity):

    def _get_source_file(self):
        source_path = self._enquiry("delta > buffer data", Keys.PROJECT_ICON)
        return Gio.File.new_for_path(source_path)

    def _get_target_file(self):
        target_directory = self._enquiry(
            "delta > buffer data",
            Keys.PROJECT_DIRECTORY
            )
        application_id = self._enquiry("delta > buffer data", Keys.PROJECT_ID)
        icon_name = "{}.svg".format(application_id)
        names = target_directory, "data", icon_name
        target_path = GLib.build_filenamev(names)
        return Gio.File.new_for_path(target_path)

    def __init__(self, parent):
        self._parent = parent
        source_file = self._get_source_file()
        target_file = self._get_target_file()
        source_file.copy(target_file, Gio.FileCopyFlags.NONE, None, None, None)
        message = MESSAGE.format(target_file.get_path())
        user_data = BufferSignals.BUFFER_APPENDED, message
        self._raise("delta > buffer signal", user_data)
