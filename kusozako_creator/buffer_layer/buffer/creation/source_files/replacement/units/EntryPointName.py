# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaEntryPointName(DeltaEntity):

    __key__ = "@KUSOZAKO_ENTRY_POINT_NAME@"

    def __init__(self, parent):
        self._parent = parent
        project_name = self._enquiry("delta > buffer data", Keys.PROJECT_NAME)
        library_name = project_name.replace("-", "_")
        if project_name != library_name:
            value = project_name+".py"
        else:
            value = "entry-point.py"
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
