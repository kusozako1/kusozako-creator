# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity


class DeltaYYYY(DeltaEntity):

    __key__ = "@KUSOZAKO_YYYY@"

    def __init__(self, parent):
        self._parent = parent
        year = GLib.DateTime.new_now_local().get_year()
        value = str(year)
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
