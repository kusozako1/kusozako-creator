# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys

REPLACEMENTS = {
    "-1": "HOME",
    "0": "DESKTOP",
    "1": "DOCUMENTS",
    "2": "DOWNLOAD",
    "3": "MUSIC",
    "4": "PICTURES",
    "5": "PUBLIC_SHARE",
    "6": "TEMPLATES",
    "7": "VIDEOS",
}
QUERY = Keys.FILES_DEFAULT_DIRECTORY


class DeltaDefaultDirectory(DeltaEntity):

    __key__ = "@KUSOZAKO_DEFAULT_DIRECTORY@"

    def __init__(self, parent):
        self._parent = parent
        buffer_data = self._enquiry("delta > buffer data", QUERY)
        value = "UserSpecialDirectories.{}".format(REPLACEMENTS[buffer_data])
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
