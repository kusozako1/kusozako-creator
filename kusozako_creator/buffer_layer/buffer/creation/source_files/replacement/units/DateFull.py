# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity

WEEK = {1: "Mon", 2: "Tue", 3: "Wed", 4: "Thu", 5: "Fri", 6: "Sat", 7: "Sun"}
MONTHS = {
    1: "Jan",
    2: "Feb",
    3: "Mar",
    4: "Apr",
    5: "May",
    6: "Jun",
    7: "Jul",
    8: "Aug",
    9: "Sep",
    10: "Oct",
    11: "Nov",
    12: "Dec",
}


class DeltaDateFull(DeltaEntity):

    __key__ = "@KUSOZAKO_DATE_FULL@"

    def __init__(self, parent):
        self._parent = parent
        date_time = GLib.DateTime.new_now_local()
        week = WEEK[date_time.get_day_of_week()]
        day = "{:01}".format(date_time.get_day_of_month())
        month = MONTHS[date_time.get_month()]
        year = date_time.get_year()
        value = "{}, {} {} {}".format(week, day, month, year)
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
