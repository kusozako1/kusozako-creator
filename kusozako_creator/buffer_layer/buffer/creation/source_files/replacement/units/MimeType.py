# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaMimeType(DeltaEntity):

    __key__ = "@KUSOZAKO_MIME_TYPE@"

    def __init__(self, parent):
        self._parent = parent
        buffer_data = self._enquiry("delta > buffer data", Keys.FILES_MIME)
        mime_type = ""
        for line in buffer_data.split("\n"):
            if len(line.split("/")) != 2:
                continue
            mime_type += "{};".format(line)
        value = "Categories={}".format(mime_type) if mime_type else "\b"
        user_data = self.__key__, str(value)
        self._raise("delta > add unit", user_data)
