# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaLibraryName(DeltaEntity):

    __key__ = "@KUSOZAKO_LIBRARY_NAME@"

    def __init__(self, parent):
        self._parent = parent
        project_name = self._enquiry("delta > buffer data", Keys.PROJECT_NAME)
        value = project_name.replace("-", "_")
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
