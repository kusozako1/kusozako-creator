# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaExec(DeltaEntity):

    __key__ = "@KUSOZAKO_EXEC@"

    def __init__(self, parent):
        self._parent = parent
        bin_name = self._enquiry("delta > buffer data", Keys.PROJECT_NAME)
        max_files = self._enquiry("delta > buffer data", Keys.FILES_MAX)
        if max_files == "0":
            value = bin_name
        elif max_files == "1":
            value = "{} %u".format(bin_name)
        else:
            value = "{} %U".format(bin_name)
        user_data = self.__key__, value
        self._raise("delta > add unit", user_data)
