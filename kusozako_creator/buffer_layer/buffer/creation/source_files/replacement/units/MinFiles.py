# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaMinFiles(DeltaEntity):

    __key__ = "@KUSOZAKO_MIN_FILES@"

    def __init__(self, parent):
        self._parent = parent
        value = self._enquiry("delta > buffer data", Keys.FILES_MIN)
        user_data = self.__key__, str(value)
        self._raise("delta > add unit", user_data)
