# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaMimeList(DeltaEntity):

    __key__ = "@KUSOZAKO_MIME_LIST@"

    def __init__(self, parent):
        self._parent = parent
        buffer_data = self._enquiry("delta > buffer data", Keys.FILES_MIME)
        mime_list = []
        for line in buffer_data.split("\n"):
            if len(line.split("/")) != 2:
                continue
            mime_list.append(line)
        value = str(mime_list) if mime_list else "None"
        user_data = self.__key__, str(value)
        self._raise("delta > add unit", user_data)
