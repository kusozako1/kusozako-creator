# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from .units.Units import EchoUnits


class DeltaReplacement(DeltaEntity):

    def replace(self, source_path, target_path):
        _, contents_byte = GLib.file_get_contents(source_path)
        contents = contents_byte.decode("utf-8")
        for key, value in self._units.items():
            contents = contents.replace(key, value)
        contents = contents.replace("\b\n", "")
        GLib.file_set_contents(target_path, contents.encode("utf-8"))

    def _delta_call_add_unit(self, user_data):
        key, value = user_data
        self._units[key] = value

    def __init__(self, parent):
        self._parent = parent
        self._units = {}
        EchoUnits(self)
