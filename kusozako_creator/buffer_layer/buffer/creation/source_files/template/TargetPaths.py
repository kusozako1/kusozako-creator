# (c) copyright 2023, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako1.Entity import DeltaEntity
from kusozako_creator.const import Keys


class DeltaTargetPaths(DeltaEntity):

    def path_for_name(self, name):
        for key, value in self._replacement.items():
            name = name.replace(key, value)
        return GLib.build_filenamev([self._directory, name])

    def _setup_replacement(self):
        self._replacement = {}
        project_name = self._enquiry("delta > buffer data", Keys.PROJECT_NAME)
        library_name = project_name.replace("-", "_")
        self._replacement["LIBRARY_NAME"] = library_name
        if project_name != library_name:
            self._replacement["BIN_NAME"] = project_name
        else:
            self._replacement["BIN_NAME"] = "entry-point"
        application_id = self._enquiry("delta > buffer data", Keys.PROJECT_ID)
        self._replacement["APPLICATION_ID"] = application_id
        self._replacement[".PYTHON"] = ".py"

    def __init__(self, parent):
        self._parent = parent
        self._directory = self._enquiry(
            "delta > buffer data",
            Keys.PROJECT_DIRECTORY
            )
        self._setup_replacement()
