# (c) copyright 2024, takedanemuru <takeda.nemuru@protonmail.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako1.Entity import DeltaEntity
from kusozako1.util import HomeDirectory
from kusozako_creator.const import BufferSignals
from .template.Template import DeltaTemplate
from .replacement.Replacement import DeltaReplacement

MESSAGE = _("Source file created to {}")


class DeltaSourceFiles(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        replacement = DeltaReplacement(self)
        template = DeltaTemplate(self)
        for source_path, target_path in template.enumerate_paths():
            replacement.replace(source_path, target_path)
            shorten_path = HomeDirectory.shorten(target_path)
            message = MESSAGE.format(shorten_path)
            user_data = BufferSignals.BUFFER_APPENDED, message
            self._raise("delta > buffer signal", user_data)
